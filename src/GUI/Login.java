package GUI;

import Database.ConnectionDB;
import Database.Email;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.scene.control.*;

import static javafx.scene.input.KeyCode.ENTER;

public class Login extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        display(primaryStage);
    }

    static void display(Stage primaryStage) {
        Stage window;
        window = primaryStage;
        window.setTitle("Administrator Trondheim El-Bike");

        //GridPane-layout
        GridPane layout = new GridPane();
        layout.setHgap(10);
        layout.setVgap(10);
        layout.setAlignment(Pos.CENTER);

        //New user link
        Hyperlink newUser = new Hyperlink("New user?");
        newUser.setOnAction(e -> NewUserEmail.display(window));

        //Email prompt
        TextField email = new TextField();
        email.setPromptText("Email");

        //Password prompt
        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        ConnectionDB c = ConnectionDB.Connect();

        //Sign in button
        Button signIn = new Button("Sign in");
        signIn.setOnAction(e -> {
            if (Email.isValidEmailAddress(email.getText()) && c.logInAdmin(email.getText(), password.getText())) {
                MainPage.display(window);
            } else {
                AlertBox.display("Error", "Email or password is wrong","Try again");
            }
        });
        signIn.setDefaultButton(true);
        signIn.setOnKeyPressed(e -> {
            if(e.getCode() == ENTER) {
                signIn.fire();
            }
        }
        );

        //Forgot password link
        Hyperlink forgotPassword = new Hyperlink("I forgot my password");
        forgotPassword.setOnAction(e -> ForgotPassword.display(window));

        //Layout
        layout.add(newUser, 0, 0);
        layout.add(email, 0, 1);
        layout.add(password, 0, 2);
        layout.add(signIn, 0, 3);
        layout.add(forgotPassword, 0, 4);

        //Scene
        Scene scene = new Scene(layout, 500, 500);
        window.setScene(scene);
        window.show();
    }
}
