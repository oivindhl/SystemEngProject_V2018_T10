package GUI;

import Database.ConnectionDB;
import Database.Email;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

class ForgotPassword {

    static void display(Stage window) {
        //Setting up db connection
        ConnectionDB c = ConnectionDB.Connect();

        //Layout
        GridPane layout = new GridPane();
        layout.setHgap(10);
        layout.setVgap(10);
        layout.setAlignment(Pos.CENTER);

        //E-mail label
        Label emailLabel = new Label("E-mail:");

        //E-mail textField
        TextField emailInput = new TextField();

        //Generate password button
        Button generateButton = new Button("Generate password");
        generateButton.setOnAction(e -> {
            if(!Email.isValidEmailAddress(emailInput.getText())){
                AlertBox.display("Error", emailInput.getText()+"is not an e-mail","Try again");
            } else if (!c.checkIfRegisteredBefore("admin", "email", emailInput.getText())) {
                AlertBox.display("Error", "E-mail not registered","Try again");
            } else {
                if(Email.newPassOldAcc(emailInput.getText())) {
                    Login.display(window);
                }
            }
        });

        //Hyperlink homePage
        Hyperlink goBack = new Hyperlink("Go back to login page");
        goBack.setOnAction(e -> Login.display(window));

        //Add and align to layout
        layout.add(emailLabel, 0 ,0);
        layout.add(emailInput, 0 ,1);
        layout.add(generateButton, 0, 2);
        layout.add(goBack, 0, 3);

        Scene scene = new Scene(layout, 500, 500);
        window.setScene(scene);
        window.show();
    }
}
