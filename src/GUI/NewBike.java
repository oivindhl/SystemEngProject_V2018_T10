package GUI;

import Database.ConnectionDB;
import Database.Email;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class NewBike extends Application{

    @Override
    public void start(Stage primaryStage) {
        display(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }


    static void display(Stage window) {
        window.setTitle("Administrator Trondheim El-Bike");
        final ComboBox emailComboBox = new ComboBox();

        //Layout
        GridPane layout = new GridPane();
        layout.setHgap(10);
        layout.setVgap(10);
        layout.setAlignment(Pos.CENTER);

        //Info label
        Label info = new Label("Register new bike");

        //Email prompt
        TextField price = new TextField();
        price.setPromptText("Price");

        //Re-enter email prompt
        TextField model = new TextField();
        model.setPromptText("Model");

        ConnectionDB db = ConnectionDB.Connect();


        //Register button
        Button registerButton = new Button("Register");
        registerButton.setOnAction(e -> {
            int priceInt = Integer.parseInt(price.getText());
            if(db.registerNewBike(priceInt, model.getText(),emailComboBox.getPromptText())){
                AlertBox.display("Bike Registration","You have registered a new bike!","Ok");
            }else {
                AlertBox.display("Bike Registration","Something went wrong","Ok");
            }
        });

        //Go to login label
        //Label goToLoginText = new Label("Already a user?");

        //Go to login hyperlink
        Hyperlink goToLogin = new Hyperlink("Go back to main page");
        goToLogin.setPadding(new Insets(0, 0, 0, 0));
        goToLogin.setOnAction(e -> MainPage.display(window));


        //Drop down
        String[] table = db.getTypes();
        String types="";
        for(int i=0;i<table.length;i++) {
            types += table[i];
        }
        emailComboBox.setPromptText("Type");
        emailComboBox.getItems().addAll(
                "Type",table[0],table[1],table[2]
        );

        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);

        //Add and align to layout
        layout.add(info, 0, 0);
        layout.add(price, 0, 1);
        layout.add(model, 0, 2);
        layout.add(registerButton, 0, 4);
        layout.add(goToLogin, 0, 5);
        layout.add(emailComboBox, 0, 3);

        //Scene
        Scene scene = new Scene(layout, 500, 500);
        window.setScene(scene);
        window.show();
    }

}

