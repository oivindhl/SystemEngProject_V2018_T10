package GUI;

import Database.Email;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import static javafx.scene.input.KeyCode.ENTER;

class NewUserEmail{
    static void display(Stage window) {
        window.setTitle("Administrator Trondheim El-Bike");

        //Layout
        GridPane layout = new GridPane();
        layout.setHgap(10);
        layout.setVgap(10);
        layout.setAlignment(Pos.CENTER);

        //Info label
        Label info = new Label("Create account");

        //Email prompt
        TextField emailInput = new TextField();
        emailInput.setPromptText("Email address");

        //Re-enter email prompt
        TextField email2Input = new TextField();
        email2Input.setPromptText("Confirm email address");

        //Register button
        Button registerButton = new Button("Register");
        registerButton.setOnAction(e -> {
            if(Email.isValidEmailAddress(emailInput.getText())) {
                AlertBox.display("Error", "Not a valid email address","Try again");
            } else if(!emailInput.getText().equals(email2Input.getText())) {
                AlertBox.display("Error", "Emails do not match","Try again");
            } else {
                Email.sendEmail(emailInput.getText());
                Login.display(window);
            }
        });
        registerButton.setDefaultButton(true);
        registerButton.setOnKeyPressed(e -> {
            if (e.getCode() == ENTER) {
                registerButton.fire();
            }
        });


        //Go to login label
        Label goToLoginText = new Label("Already a user?");

        //Go to login hyperlink
        Hyperlink goToLogin = new Hyperlink("Sign in");
        goToLogin.setPadding(new Insets(0, 0, 0, 82));
        goToLogin.setOnAction(e -> Login.display(window));

        //Add and align to layout
        layout.add(info, 0, 0);
        layout.add(emailInput, 0, 1);
        layout.add(email2Input, 0, 2);
        layout.add(registerButton, 0, 3);
        layout.add(goToLoginText, 0, 4);
        layout.add(goToLogin, 0, 4);


        //Scene
        Scene scene = new Scene(layout, 500, 500);
        window.setScene(scene);
    }
}
