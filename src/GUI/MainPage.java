package GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sun.rmi.runtime.Log;

public class MainPage {

    public static void display(Stage window) {
        //Main layout
        BorderPane mainLayout = new BorderPane();

        //Layout for the center
        GridPane layout = new GridPane();
        layout.setVgap(10);
        layout.setHgap(10);

        //Layout for top
        GridPane topLayout = new GridPane();
        topLayout.setHgap(20);

        //Layout for bottom
        VBox bottomLayout = new VBox();

        //Bike label
        Label bikeLabel = new Label("Bikes:");
        layout.add(bikeLabel, 0, 0);

        //The four buttons underneath bike label
        Button registerBikeButton = new Button("Register Bike");
        registerBikeButton.setOnAction(event -> NewBike.display(window));
        layout.add(registerBikeButton, 0, 1);
        Button editBikeButton = new Button("Edit/delete Bike");
        layout.add(editBikeButton, 0, 2);
        Button statusButton = new Button("BikeStatus");
        layout.add(statusButton, 0, 3);
        Button registerRepairButton = new Button("Register Repair");
        layout.add(registerRepairButton, 0, 4);

        //Docking station label
        Label stationLabel = new Label("Docking Stations:");
        layout.add(stationLabel, 2, 0);

        //Four dock related buttons
        Button registerDockButton = new Button("Register Dock");
        layout.add(registerDockButton, 2, 1);
        Button editDockButton = new Button("Edit/delete Dock");
        layout.add(editDockButton, 2, 2);
        Button dockStatusButton = new Button("Dock Status");
        layout.add(dockStatusButton, 2, 3);
        Button mapButton = new Button("Dock Map");
        layout.add(mapButton, 2, 4);

        //Setting alignment and adding to main layout
        layout.setAlignment(Pos.CENTER);
        mainLayout.setCenter(layout);

        //Logout button
        Button logoutButton = new Button("Logout");
        logoutButton.setOnAction(e -> Login.display(window));

        //Adding button to bottomLayout, setting alignment, adding to main
        bottomLayout.getChildren().add(logoutButton);
        bottomLayout.setAlignment(Pos.BOTTOM_LEFT);
        mainLayout.setBottom(bottomLayout);

        //Adding hyperlinks for user and statistical information
        Hyperlink userInfoLink = new Hyperlink("User Info");
        //userInfoLink.setOnAction(e -> UserInfo.display(window));
        Hyperlink statisticalInfoLink = new Hyperlink("Statistical Information");
        //statisticalInfoLink.setOnAction(e -> StatisticalInfo.display(window));

        topLayout.add(userInfoLink, 0, 0);
        topLayout.add(statisticalInfoLink, 1, 0);
        topLayout.setAlignment(Pos.TOP_CENTER);
        mainLayout.setTop(topLayout);

        Scene scene = new Scene(mainLayout, 500, 500);
        window.setScene(scene);
        window.show();
    }
}