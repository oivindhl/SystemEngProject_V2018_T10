package Database;

import java.sql.*;

public class Cleaner {
    static void closeResultSet(ResultSet result){
        try {
            if (result != null) {
                result.close();
            }
        }catch (SQLException sqle){
            writeMessage(sqle, "closeResultSet()");
        }
    }

    static void closeStatement(Statement statement){
        try{
            if(statement != null){
                statement.close();
            }
        }catch (SQLException sqle){
            writeMessage(sqle, "closeStatement()");
        }
    }

    public static void closeConnection(Connection connection){
        try {
            if( connection != null){
                connection.close();
            }
        }catch (SQLException sqle){
            writeMessage(sqle, "closeConnection()");
        }
    }

    static void rollBack(Connection connection){
        try{
            if(connection != null && !connection.getAutoCommit()){
                connection.setAutoCommit(true);
            }
        }catch (SQLException sqle){
            writeMessage(sqle, "rollBack()");
        }
    }

    static void settAutoCommit(Connection connection){
        try{
            if(connection != null && !connection.getAutoCommit()){
                connection.setAutoCommit(true);
            }
        }catch (SQLException sqle){
            writeMessage(sqle,"settAutoCommit()");
        }
    }

    static void writeMessage(Exception e, String message){
        System.err.println("Error: "+ message+".");
        e.printStackTrace(System.err);
    }
}
