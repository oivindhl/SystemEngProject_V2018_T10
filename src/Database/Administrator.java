package Database;

import org.apache.commons.codec.binary.Base64;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;


public class Administrator {
    private static final Random RANDOM = new SecureRandom();
    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    public static String getHash(byte[] inputBytes,String algorithm,byte[] salt){
        String hashValue="";
        try{
            MessageDigest messageDigest= MessageDigest.getInstance(algorithm);
            messageDigest.update(salt);
            byte[] digestedBytes = messageDigest.digest(inputBytes);
            hashValue = DatatypeConverter.printHexBinary(digestedBytes).toLowerCase();
        }catch (Exception e){

        }
        return hashValue;
    }

    public static byte[] getNextSalt() {
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return salt;
    }

    public static String bytetoString(byte[] input) {
        return org.apache.commons.codec.binary.Base64.encodeBase64String(input);
    }

    public static byte[] stringToByte(String input) {
        if (Base64.isBase64(input)) {
            return Base64.decodeBase64(input);

        } else {
            return Base64.encodeBase64(input.getBytes());
        }
    }

    public static String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    /*public static void main(String[] args){
        String password= "hello";
        String p = "stian@hotmail.com";
        String h = password+p;
        byte[] salt = {100,100,100,100,100,100,7,8,9,10,11,12,13,14,15,16};
        byte[] salt1 = {1,2,3,5,5,6,7,8,9,10,11,12,13,14,15,16};
        String hash = getHash(h.getBytes(),"SHA-512",getNextSalt());
        String hash2 = getHash(p.getBytes(),"SHA-512",salt1);
        String test = bytetoString(salt);
        /*System.out.println(test);
        byte[] hei = stringToByte(test);
        for(int i=0;i<hei.length;i++){
            System.out.println(hei[i]);
        }
        String random = randomString(10);
        System.out.println(random);
    }*/
}
