package Database;

import java.security.SecureRandom;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

import static Database.ConnectionDB.Connect;

public class Email {

    private static final String host = "smtp.gmail.com";
    private static final String user = "team10NTNU@gmail.com";//change accordingly
    private static final String password = "10teamteam10";//change accordingly
    private static final String SSLPORT = "465";

    private static Session setup(){
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.socketFactory.port", SSLPORT);
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", SSLPORT);

        return Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, password);
                    }
                });

    }


    private static String generateString(){
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        int length = 10;
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++){
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }


    public static boolean sendEmail(String email) {

        Session session = setup();

        try {
            String newPass = generateString();
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Password");
            message.setText("Here is your automatically generated password. It is recommended that you change it immediately.\n" +
                    "Password: " + newPass + "\n\n\n\n\n" +
                    "-------------------------------------------------------\n" +
                    "This mail is being sent from Trondheim El-bike Rental. If you need help or want to contact us, please reach us at: " + user);

            ConnectionDB db = Connect();
            if (!(db.checkIfRegisteredBefore("admin", "email",email))){
                if (db.registerNewAdmin(email, newPass)) {
                    Transport.send(message);
                    return true;
                }
            }

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean newPassOldAcc(String email){

        Session session = setup();

        try {
            String newPass = generateString();
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Password");
            message.setText("Here is your automatically generated password. It is recommended that you change it immediately.\n" +
                    "Password: " + newPass + "\n\n\n\n\n" +
                    "-------------------------------------------------------\n" +
                    "This mail is being sent from Trondheim El-bike Rental. If you need help or want to contact us, please reach us at: " + user);

            ConnectionDB db = Connect();
            if (db.changePasswordForgotten(email, newPass)) {
                Transport.send(message);
                return true;
            }


        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return false;

    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static void main (String[] args){
        System.out.println(sendEmail("stian_adnanes@hotmail.com"));
    }
}

