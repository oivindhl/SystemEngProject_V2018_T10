package Database;

import java.sql.*;
import java.util.ArrayList;

public class ConnectionDB {
    private Connection connection;

    private ConnectionDB(String dbDriver, String dbName) throws Exception {
        try{
            Class.forName(dbDriver);
            connection = DriverManager.getConnection(dbName);
        }catch (Exception e){
            Cleaner.writeMessage(e, "Constructor");
            throw e;
        }
    }

    public boolean registerNewBike(int price, String make, String type){
        Statement statement = null;
        final byte CHARGING_LEVEL = 100;
        boolean registeredOk = false;
        try{
            String insertBike = "INSERT INTO bike (bikeID, bikePrice, bikeDate, chargingLevel, modelname) VALUES (DEFAULT,'"+price+"', CURDATE(),'"+CHARGING_LEVEL+"', '"+make+"')";
            statement = connection.createStatement();
            if(!checkIfRegisteredBefore("model","modelName",make)){
                connection.setAutoCommit(false);
                if(registerNewModel(make,type) && statement.executeUpdate(insertBike)!=0){
                    connection.commit();
                    registeredOk = true;
                }
            } else{
                if(statement.executeUpdate(insertBike)!=0){
                    registeredOk = true;
                }
            }

        }catch (SQLException sqle){
            Cleaner.writeMessage(sqle, "registrerNewBike()");
            Cleaner.rollBack(connection);
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.settAutoCommit(connection);
        }
        return registeredOk;
    }

    private boolean registerNewTrip(int bikeID){
        Statement statement = null;
        ResultSet result = null;
        String getLastID = "SELECT MAX(tripID)AS max FROM trip WHERE bikeID='"+bikeID+"';";
        boolean registeredOk = false;

        try{
            if(checkIfRegisteredBefore("bike","bikeID",bikeID)){
                statement = connection.createStatement();
                result = statement.executeQuery(getLastID);
                result.next();
                int newID = result.getInt("max") + 1;
                String sqlStatement = "INSERT INTO trip(tripID,bikeID) VALUES('"+newID+"','"+bikeID+"');";
                statement.executeUpdate(sqlStatement);
                registeredOk = true;
            }
        }catch(SQLException sqle){
            Cleaner.writeMessage(sqle,"registerNewTrip()");
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeResultSet(result);
        }
        return registeredOk;
    }

    public boolean registerNewRepair(String requestDescription, int bikeID){
        Statement statement = null;
        String sqlStatement = "INSERT INTO repairs(dateSent,requestDescription,bikeID) VALUES(CURDATE(),'"+requestDescription+"','"+bikeID+"')";
        boolean registeredOk = false;
        try{
            if(checkIfRegisteredBefore("bike","bikeID",bikeID)) {
                statement = connection.createStatement();
                if (statement.executeUpdate(sqlStatement) != 0) {
                    registeredOk = true;
                }
            }
        }catch (SQLException sqle){
            Cleaner.writeMessage(sqle, "registerNewRepair");
        }finally {
            Cleaner.closeStatement(statement);
        }
        return registeredOk;
    }

    private boolean registerNewType(String type){
        Statement statement = null;
        boolean registeredOk = false;
        String sqlStatement = "INSERT INTO type (typename) VALUES('"+type+"')";
        try{
            if(!checkIfRegisteredBefore("type","typename",type)){
                statement = connection.createStatement();
                if(statement.executeUpdate(sqlStatement)!=0){
                    registeredOk=true;
                }
            }
        }catch(SQLException sqle){
            Cleaner.writeMessage(sqle,"registerNewType");
        }finally {
            Cleaner.closeStatement(statement);
        }
        return registeredOk;
    }

    private boolean registerNewModel(String model, String type){
        Statement statement = null;
        boolean registeredOk = false;
        String sqlStatement = "INSERT INTO model VALUES('"+model+"','"+type+"')";
        try{
            statement = connection.createStatement();
            if(!checkIfRegisteredBefore("model","modelName",model)){
                if(!checkIfRegisteredBefore("type","typename",type)) {
                    connection.setAutoCommit(false);
                    if(statement.executeUpdate(sqlStatement)!=0 && registerNewType(type)){
                        registeredOk=true;
                        connection.commit();
                    }
                }else {
                    if(statement.executeUpdate(sqlStatement)!=0) {
                        registeredOk = true;
                    }
                }
            }
        }catch(SQLException sqle){
            Cleaner.writeMessage(sqle,"registerNewType");
            Cleaner.rollBack(connection);
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.settAutoCommit(connection);
        }
        return registeredOk;
    }

    public String[] getTypes(){
        Statement statement = null;
        String sqlStatement = "SELECT * FROM type";
        ResultSet res = null;
        ArrayList<String> out = new ArrayList<>();
        try {
            statement = connection.createStatement();
            res = statement.executeQuery(sqlStatement);
            while (res.next()){
                out.add(res.getString("typename"));
            }
            String[] outTable = new String[out.size()];
            for (int i = 0; i < out.size(); i++){
                outTable[i] = out.get(i);
            }
            return outTable;
        }catch(SQLException sqle){
            Cleaner.writeMessage(sqle,"getTypes");
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeResultSet(res);
        }
        return null;
    }

    public boolean checkIfRegisteredBefore(String nameOfTable, String columnName, Object value){
        Statement statement = null;
        ResultSet result = null;
        boolean regBefore = false;
        String sqlStatement= "SELECT * FROM "+nameOfTable+" WHERE "+columnName+"='"+value+"';";
        try{
            statement = connection.createStatement();
            result = statement.executeQuery(sqlStatement);
            if(result.next()){
                regBefore = true;
            }
        } catch (SQLException sqle){
            Cleaner.writeMessage(sqle, "checkType");
        }finally {
            Cleaner.closeResultSet(result);
            Cleaner.closeStatement(statement);
        }
        return regBefore;
    }

    boolean registerBikeData(int bikeID, byte chargingLevel, double latitude, double longitude) {
        Statement statement = null;
        boolean ok = false;
        String sqlStatement = "INSERT INTO bikeData (bikeID, chargingLevel, latitude, longitude, currentTime) VALUES('"+bikeID+"', '"+chargingLevel+"', '"+latitude+"', '"+longitude+"', NOW())";
        try{
            statement = connection.createStatement();
            connection.setAutoCommit(false);
            if(statement.executeUpdate(sqlStatement) != 0) {
                connection.commit();
                ok = true;
            }
        } catch(SQLException sqle) {
            Cleaner.closeStatement(statement);
            Cleaner.settAutoCommit(connection);
            ok = false;
        } finally {
            Cleaner.closeStatement(statement);
            Cleaner.settAutoCommit(connection);
        }
        return ok;
    }

    public boolean registerNewDockingStation(String name, short powerUsage, byte maxBikes, double latitude, double longitude) {
        PreparedStatement statement = null;
        String sqlStatement = "INSERT INTO dockingStation(dockingID, name, powerUsage, maxBikes, latitude, longitude) VALUES(DEFAULT, ?, ?, ?, ?, ?)";
        boolean ok = false;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(sqlStatement);
            statement.setString(1, name);
            statement.setShort(2, powerUsage);
            statement.setByte(3, maxBikes);
            statement.setDouble(4, latitude);
            statement.setDouble(5, longitude);
            if(statement.executeUpdate() != 0) {
                connection.commit();
                ok = true;
            }
        } catch(SQLException sqle) {
            Cleaner.rollBack(connection);
            ok = false;
        } finally {
            Cleaner.closeStatement(statement);
            Cleaner.settAutoCommit(connection);
        }
        return ok;
    }

    boolean registerNewAdmin(String email, String password){
        Statement statement = null;
        byte[] salt = Administrator.getNextSalt();
        String saltString = Administrator.bytetoString(salt);
        //String userAndPassword = email+password;
        String hash = Administrator.getHash(password.getBytes(),"SHA-512",salt);
        String sqlStatement = "INSERT INTO admin VALUES('"+email+"','"+hash+"','"+saltString+"')";
        boolean registeredOk = false;

        try{
            if(!checkIfRegisteredBefore("admin","email",email)){
                statement = connection.createStatement();
                if(statement.executeUpdate(sqlStatement)!=0){
                    registeredOk = true;
                }
            }
        }catch (SQLException sqle){
            Cleaner.writeMessage(sqle,"registerNewAdmin()");
        }finally {
            Cleaner.closeStatement(statement);
        }
        return registeredOk;
    }

    public boolean logInAdmin(String email, String password){
        Statement statement = null;
        ResultSet result = null;
        //String userAndPassword = email+password;
        String getSalt = "SELECT * FROM admin WHERE email='"+email+"'";
        boolean okLogIn = false;

        try{
            if(checkIfRegisteredBefore("admin","email",email)){
                statement = connection.createStatement();
                result = statement.executeQuery(getSalt);
                result.next();
                String salt = result.getString("salt");
                byte[] saltTable = Administrator.stringToByte(salt);
                String hash = result.getString("hash");
                String okHash = Administrator.getHash(password.getBytes(),"SHA-512", saltTable);
                if(okHash.equals(hash)){
                    okLogIn = true;
                }
            }
        }catch (SQLException sqle){
            Cleaner.writeMessage(sqle, "logInAdmin");
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeResultSet(result);
        }
        return okLogIn;
    }

    public boolean changePassword(String email, String currentPassword, String newPassword){
        Statement statement = null;
        ResultSet result = null;
        String getSalt = "SELECT * FROM admin WHERE email='"+email+"'";
        boolean okLogIn = false;

        try{
            if(checkIfRegisteredBefore("admin","email",email)){
                statement = connection.createStatement();
                result = statement.executeQuery(getSalt);
                result.next();
                String salt = result.getString("salt");
                byte[] saltTable = Administrator.stringToByte(salt);
                String hash = result.getString("hash");

                String okHash = Administrator.getHash(currentPassword.getBytes(),"SHA-512", saltTable);
                byte[] newSalt = Administrator.getNextSalt();
                String newSaltString = Administrator.bytetoString(newSalt);

                String newHash = Administrator.getHash(newPassword.getBytes(),"SHA-512",newSalt);
                String setNewPassword="UPDATE admin SET hash='"+newHash+"', salt='"+newSaltString+"' WHERE hash='"+hash+"'";
                if(okHash.equals(hash)){
                    if(statement.executeUpdate(setNewPassword)!=0) {
                        okLogIn = true;
                    }
                }
            }
        }catch (SQLException sqle){
            Cleaner.writeMessage(sqle, "changePassword");
        }finally {
            Cleaner.closeStatement(statement);
            Cleaner.closeResultSet(result);
        }
        return okLogIn;
    }

    public boolean changePasswordForgotten(String email, String newPassword){
        Statement statement = null;
        boolean okChange = false;

        try{
            if(checkIfRegisteredBefore("admin","email",email)){
                statement = connection.createStatement();

                byte[] newSalt = Administrator.getNextSalt();
                String newSaltString = Administrator.bytetoString(newSalt);

                String newHash = Administrator.getHash(newPassword.getBytes(),"SHA-512",newSalt);
                String setNewPassword="UPDATE admin SET hash='"+newHash+"', salt='"+newSaltString+"' WHERE email ='"+email+"'";
                if(statement.executeUpdate(setNewPassword)!=0) {
                    okChange = true;
                }

            }
        }catch (SQLException sqle){
            Cleaner.writeMessage(sqle, "changePassword");
        }finally {
            Cleaner.closeStatement(statement);
        }
        return okChange;
    }

    public static ConnectionDB Connect(){
        String dbDriver = "com.mysql.jdbc.Driver";
        String databasenavn = "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/sandereh?user=sandereh&password=hyHJ1IDm";
        ConnectionDB d = null;
        try{
            d = new ConnectionDB(dbDriver, databasenavn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    public static void main(String[] args){
        //ConnectionDB d = Connect();
        //boolean ok = logInAdmin("williaan@hotmail.com","stianekul");
        //boolean ok = d.changePassword("williaan@hotmail.com","ikkjekul","stianekul");
        //System.out.println(ok);
    }
}